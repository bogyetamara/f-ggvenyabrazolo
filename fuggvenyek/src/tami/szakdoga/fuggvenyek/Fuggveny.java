package tami.szakdoga.fuggvenyek;

import java.util.Stack;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.graphics.Canvas;
import android.widget.Toast;

public class Fuggveny {
	String keplet;
	String lengyel;
	
	Object[] tagok;
	
	Style stilus;
	
	//(x^2+3)^0.5 - 6
	//x 2 ^ 3 + 0.5 ^ 6 -
	
	
	public void DrawOnto(Canvas canv)
	{
		//TODO
	}
	
	public double getValueAt(double x)
	{
		//TODO
		return 0;
	}
	
	protected static String Rendez(String[] strs, int k, int v)
	{
		boolean t = false;
		Stack<String> stack = new Stack<String>();
		
		for(int i = k; i < v; i++)
		{
			try{
				stack.push("" + Double.parseDouble(strs[i]));				
				continue;
			} catch(Exception e){}
			
			if(strs[i].equals("x"))
				stack.push("x");

			if(strs[i].equals("(") || strs[i].equals("-("))
			{
				//Keres
			}
				
			if(strs[i].equals("+"))
			{
				
			}
		}
		return null;
	}
	
	public static String Reorder(String str)
	{
		//TODO
		String[] strs = str.split(" ");
		return Rendez(strs, 0, strs.length);
	}
	
	public void Feldolgoz()
	{
		lengyel = Reorder(keplet);
		
		tagok = lengyel.split(" ");
		
		//TODO �rtelmez tagok -> �talak�t�s (Double.parseDouble(tagok[i]))
		for(int i  = 0; i < tagok.length; i++)
		try{
			tagok[i] = Double.parseDouble((String)tagok[i]);
		}
		catch(Exception e){tagok[i] = (char) tagok[i];}
	}
	
	public double Evaluate(double x) throws TulSokTagHiba, ErtelmezesiHiba
	{
		if(lengyel == null) lengyel = Reorder(keplet);
		
		EvalLengyel(x);
		return 0;
	}
	
	public double EvalLengyel(double x) throws TulSokTagHiba, ErtelmezesiHiba
	{
		//TODO
		
		int index = 0;
		
		Stack<Double> a = new Stack<Double>();
		
		while(index < tagok.length)
		{
			if((tagok[index]) instanceof Double)
			{
				a.add((Double) tagok[index]);
				index++;
				continue;
			}
			
			Double t1, t2;
			
			switch((char) tagok[index])
			{
			case 'x':
				a.add(x);
				break;
			case '+':
				if(a.size() < 2) throw new ErtelmezesiHiba();
				a.push(a.pop() + a.pop());
				break;
			case '-':
				if(a.size() < 2) throw new ErtelmezesiHiba();
				a.push(-a.pop() + a.pop());
				break;
			case '*':
				if(a.size() < 2) throw new ErtelmezesiHiba();
				a.push(a.pop() * a.pop());
				break;
			case '/':
				if(a.size() < 2) throw new ErtelmezesiHiba();
				t2 = a.pop();
				t1 = a.pop();
				a.push(t1 / t2);
				break;
			case '^':
				if(a.size() < 2) throw new ErtelmezesiHiba();
				t2 = a.pop();
				t1 = a.pop();
				a.push(Math.pow(t1,t2));
				break;
			}
			index++;
		}
		
		//Ha t�l sok tagunk maradt (nem 1), akkor valami gond van (pl 1 2 + 3, ami annak felel meg, hogy 1+2 3 aminek nincs �rtelme, mert nincs m�velet 2 tagunk k�z�tt) 
		if(a.size() > 1) throw new TulSokTagHiba();
		
		return a.pop();
	}
}
