package tami.szakdoga.fuggvenyek;

import java.util.ArrayList;
import java.util.Locale;

import tami.szakdoga.fuggvenyek.GraphView.GraphData;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.text.style.SuperscriptSpan;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;



public class MainActivity extends ActionBarActivity {
	
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	private ArrayList<Graph> graphs = new ArrayList<Graph>();
	private ArrayList<GraphData> gdata = new ArrayList<GraphData>();
	private Graph settingsgraph = null;
	private ArrayAdapter<Graph> listAdapter;
	private PlaceholderFragment settingspage = null;
	
	private int selectedGraph = -1;
	
	//Kiment minden inform�ci�t
	@Override
	public void onSaveInstanceState (Bundle outState)
	{
		super.onSaveInstanceState(outState);
		//TODO
		outState.putInt("oldalszam", mSectionsPagerAdapter.count);
		outState.putParcelableArrayList("fvek", graphs);
		outState.putParcelableArrayList("koordrendsz", gdata);
		outState.putParcelable("fv_beall", settingsgraph);
	}
	
	public void addPage()
	{
		mSectionsPagerAdapter.addPage();
		gdata.add(new GraphData(getPageCount()-2));
	}
	
	public void addGraph()
	{
		graphs.add(new Graph(CONST.FV_CONST, new float[]{0}));
		
		refreshList();
	}
	
	public void refreshList()
	{/*
		listAdapter.clear();
		//Nem volt hajlando automatikusan friss�lni...
		for(int i = 0; i < graphs.size(); i++)
			listAdapter.add(graphs.get(i));
		*/
		listAdapter.notifyDataSetChanged();
	}
	
	public void ChangeSelectedGraph(int id)
	{
		selectedGraph = id;
		if(id == -1)
			settingsgraph = null;
		else
			settingsgraph = graphs.get(id).Clone();
		
		if(settingspage != null)
			settingspage.ReloadSettings();
	}
	
	public void removeGraph(int i)
	{
		graphs.remove(i);//kiszedj�k
		if(i < selectedGraph) selectedGraph--;//Ha olyat szedt�nk ki, ami el�r�bb volt, akkor el�r�bb ker�lt a kiv�lasztott
		else if(i== selectedGraph) ChangeSelectedGraph(-1);//ha pont a kiv�lasztottat szedt�k ki, akkor nincs kiv�lasztott
		
		refreshList();
	}
	
	public void removePage(int i)
	{
		mSectionsPagerAdapter.removePage();
		
		//N�zz�k meg mi t�rt�nik a kiv�laszott fv-el miel�tt m�g �jra�rjuk az oldalsz�mokat
		if(selectedGraph != -1 && graphs.get(selectedGraph).getPage() == i)
		{
			ChangeSelectedGraph(-1);
		}
		
		for(int j = 0; j < graphs.size(); j++)
		{
			//Cs�kken az oldalsz�m
			if(graphs.get(j).getPage() > i)
				graphs.get(j).changePage(graphs.get(j).getPage()-1);
			else
				//Ha pont azon volt, akkor kidobjuk 
				if(graphs.get(j).getPage() == i)
				{
					graphs.remove(j);//kiszedj�k, ha nem kell
					j--;//Kiszedt�nk egyet, el�r�bb cs�szott h�tulr�l minden
					if(j < selectedGraph) selectedGraph--;//Ha olyat szedt�nk ki, ami el�r�bb volt, akkor el�r�bb ker�lt a kiv�lasztott 
				}
		}
		gdata.remove(i-1);

		refreshList();
		//mViewPager.requestLayout();
	}
	
	public int getPageCount()
	{
		return mSectionsPagerAdapter.count;
	}
	
	GraphData getGraphData(int pagenum)
	{
		return gdata.get(pagenum);
	}

	void setGraphData(int pagenum, GraphData data)
	{
		if(pagenum >= 0 && pagenum < gdata.size())
			gdata.set(pagenum, data);
	}
	
	ArrayList<Graph> getGraphs()
	{
		return graphs;
	}
	
	Graph getCurrentSettingsGraph()
	{
		return settingsgraph;
	}
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//TODO ez ide nem kell
		graphs.add(new Graph(CONST.FV_LIN, new float[]{1,0 ,23}));
		graphs.add(new Graph(CONST.FV_LIN, new float[]{2,-1 ,3}));
		graphs.add(new Graph(CONST.FV_LIN, new float[]{-1,0 ,12}));
		graphs.add(new Graph(CONST.FV_LIN, new float[]{-3,4 ,-3}));
		graphs.add(new Graph(CONST.FV_LIN, new float[]{2,-1 ,3}));
		graphs.add(new Graph(CONST.FV_LIN, new float[]{-1,0 ,12}));
		graphs.add(new Graph(CONST.FV_LIN, new float[]{-3,4 ,-3}));
		Graph g = new Graph(CONST.FV_CONST, new float[]{1.4f});

		g.setColor(1);
		graphs.add(g);
		
		
		listAdapter = new ArrayAdapter<Graph>(this, android.R.layout.simple_list_item_1, graphs);
		
		//mViewPager = new CustomViewPager(this);
		setContentView(R.layout.activity_main);
		//this.setContentView(mViewPager);
		
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
		
		//Ha vannak mentett adataink akkor �ll�tsuk vissza
		if(savedInstanceState != null)
		{
			graphs = savedInstanceState.getParcelableArrayList("fvek");
			gdata = savedInstanceState.getParcelableArrayList("koordrendsz");
			mSectionsPagerAdapter.count = savedInstanceState.getInt("oldalszam");
			settingsgraph = savedInstanceState.getParcelable("fv_beall");
		}
		else//Ha nincs mentett adatunk, akkor hozzunk l�tre 1 oldalt
		{
			addPage();
			addPage();
		}
		
		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
	}

	
	//Ha kijel�l�nk valamit a list�b�l - long touch
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	  if (v.getId()==R.id.listView1) {
		  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
		  menu.setHeaderTitle(graphs.get(info.position).toString());
		  String[] menuItems = {"T�r�l", "V�ltoztat"};
		  for (int i = 0; i<menuItems.length; i++) {
		      menu.add(Menu.NONE, i, i, menuItems[i]);
		  }
	  }
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	  int menuItemIndex = item.getItemId();
	  switch(menuItemIndex)
	  {
	  case 0:
		  removeGraph(info.position);
		  break;
	  case 1:
		  ChangeSelectedGraph(info.position);
		  
		  break;
	  }
	  return true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id)
		{
		case R.id.uj_fv:
			addGraph();
			break;
		case R.id.uj_oldal:
			addPage();
			break;
		case R.id.torol_oldal:
			int ind = mViewPager.getCurrentItem() -1;
			if(ind > 0)
				removePage(ind);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		private int count = 2;//Az a 2 page, amin a be�ll�t�sok vannak
		
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			return PlaceholderFragment.newInstance(position + 1);
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return count;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
		
		public void addPage()
		{
			count++;
			this.notifyDataSetChanged();
		}
		
		public void removePage()
		{
			count--;
			this.notifyDataSetChanged();
		}
	}

	public static class PlaceholderFragment extends Fragment {
				
		private int pagenum = 0;
		private GraphView graph = null;//Csak akkor kell, ha van neki egy�ltal�n
		private View settings_view = null;//Az a view amiben a be�ll�t�sok vannak, amit ut�lag t�lt be
		private boolean spinner_def_select = true;
		
		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(CONST.ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		
		View LoadPage(LayoutInflater inflater, ViewGroup container)
		{
			View rootView;
			
			switch (pagenum)
			{
			case 1:
				rootView = inflater.inflate(R.layout.fuggveny_oldal, container, false);
				LoadHomePage(rootView);
				break;
			case 2:
				rootView = inflater.inflate(R.layout.graph_settings, container, false);
				LoadGraphSettingsPage((LinearLayout) rootView);
				break;
			default:
				rootView = LoadGraphPage(container.getContext(), pagenum-2);
				break;
			}
			return rootView;
		}
		
		void LoadHomePage(View root)
		{
			ListView list = (ListView) root.findViewById(R.id.listView1);
			list.setAdapter(((MainActivity) getActivity()).listAdapter);
			registerForContextMenu(list);
		}
		
		@SuppressLint("InflateParams")
		void LoadGraphSettingsPage(LinearLayout root)
		{
			//Mondjuk meg az activitynek, hogy l�tre lett�nk hozva, �s itt vagyunk
			((MainActivity) getActivity()).settingspage = this;
			
			//Mert a v�g�n csak egy maradhat
			if(settings_view != null)
				root.removeView(settings_view);
			for(int i = 0; i < root.getChildCount(); i++)
				if((root.getChildAt(i) instanceof Button && ((Button) root.getChildAt(i)).getText() == "Ment") || (root.getChildAt(i) instanceof TextView && ((TextView) root.getChildAt(i)).getText() == "Nincs kiv�lasztott f�ggv�ny!"))
					{
						root.removeView(root.getChildAt(i));
					}
			
			
			
			Graph g = ((MainActivity) getActivity()).getCurrentSettingsGraph();
			
			if(g == null)
			{
				TextView asd = new TextView(root.getContext());
				asd.setText("Nincs kiv�lasztott f�ggv�ny!");
				asd.setTextSize(20);
				root.addView(asd);
				return;
			}
			
			//Ki�rjuk a f�ggv�nyt
			String text = g.toString();
			
			EditText oldalszam = (EditText) root.findViewById(R.id.page_num);
			oldalszam.setText("" + g.getPage());
			
			Spinner spin = (Spinner) root.findViewById(R.id.tipus);
			
			//Szerezz�k meg a megfelel� be�ll�t�smez�t
			switch(g.getType())
			{
			case CONST.FV_CONST:
				spin.setSelection(0);
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.const_fv, null);
				setParams(settings_view, 1, g.getParams());
				break;
			case CONST.FV_LIN:
				spin.setSelection(1);
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.lin_fv, null);
				setParams(settings_view, 3, g.getParams());
				break;
			case CONST.FV_NFOK:
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.nfoku, null);
				setParams(settings_view, 4, g.getParams());
				break;
			case CONST.FV_GYOK:
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.gyok, null);
				setParams(settings_view, 3, g.getParams());
				break;
			case CONST.FV_ABS:
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.abs, null);
				setParams(settings_view, 3, g.getParams());
				break ;
			case CONST.FV_EGESZ:
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.egesz, null);
				setParams(settings_view, 3, g.getParams());
				break ;
			case CONST.FV_TORT:
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.tort, null);
				setParams(settings_view, 3, g.getParams());
				break ;
			case CONST.FV_SIG:
				settings_view = getActivity().getLayoutInflater().inflate(R.layout.signum, null);
				setParams(settings_view, 3, g.getParams());
				break ;
			}
			
			spin.setOnItemSelectedListener(new OnItemSelectedListener() {
			    @Override
			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			        // your code here
			    	if(spinner_def_select)
						spinner_def_select = false;						
					else
						ChangeType(((TextView)selectedItemView).getText().toString());
			    }

			    @Override
			    public void onNothingSelected(AdapterView<?> parentView) {
			        // your code here
			    }
			});
			
			root.addView(settings_view);
			
			//F�ggv�ny sz�vege
			((TextView) root.findViewById(R.id.textView1)).setText(text);
			
			//Sz�n
			((Spinner)root.findViewById(R.id.szin)).setSelection(g.getColor());
						
			//Ment�s gomb
			Button butt = new Button(getActivity());
			butt.setText("Ment");
			
			butt.setOnClickListener(new OnClickListener(){
					public void onClick(View v) {
						SaveSettings();
					}
			});
			root.addView(butt);
		}
		
		void ChangeType(String to)
		{
			float[] p = null;
			switch(to)
			{
			case CONST.FV_CONST:
				p = new float[1];
				break;
			case CONST.FV_LIN:
				p = new float[3];
				break;
			case CONST.FV_NFOK:
				p = new float[4];
				break ;
			case CONST.FV_GYOK:
				p = new float[3];
				break ;
			case CONST.FV_ABS:
				p = new float[3];
				break ;
			case CONST.FV_EGESZ:
				p = new float[3];
				break ;
			case CONST.FV_TORT:
				p = new float[3];
				break ;
			case CONST.FV_SIG:
				p = new float[3];
				break ;
			}
			
			//Ha nincs mit �ll�tani akkor nincs mir�l besz�lni
			if(((MainActivity) getActivity()).getCurrentSettingsGraph() == null)
			{
				ReloadSettings();
				return;
			}
			
			p[0] = 1;
			
			//Amennyiben v�ltottunk t�pust alapb�l �rz�keli a v�lt�st, �s azt hiszi a user csin�lta, de val�j�ban nem t�rt�nt t�pusv�lt�s
			//Ilyenkor t�lts�k vissza az alap�rtelmezett dolgokat (meg �ltal�ban amikor ugyanarra v�ltunk vissza ami eredetileg volt)
			MainActivity act = ((MainActivity) getActivity());
			if(act.getGraphs().get(act.selectedGraph).getType().equals(to))
				p = act.getGraphs().get(act.selectedGraph).getParams();
			
			((MainActivity)getActivity()).settingsgraph.changeType(to, p);
			ReloadSettings();
		}
		
		void ReloadSettings()
		{
			LinearLayout ll =  (LinearLayout) this.getView().findViewById(R.id.settings_layout);
			LoadGraphSettingsPage(ll);
		}
		
		void setParams(View ll, int db, float[] params)//visszaadja az n. param�tert
		{
			for(int i = 0; i < db; i++)
			{
				int elojel_id = getResources().getIdentifier("elojel_" + i, "id", "tami.szakdoga.fuggvenyek");
				if(elojel_id != 0)//Csak ha van el�jel�nk
					{
					ToggleButton tb = ((ToggleButton)ll.findViewById(elojel_id));
					if(tb != null)
						tb.setChecked(params[i] >= 0);
					}
					
				
				
				int szam_id = getResources().getIdentifier("param_" + i, "id", "tami.szakdoga.fuggvenyek");
				((EditText)ll.findViewById(szam_id)).setText("" + Math.abs(params[i]));
			}
		}
		
		float[] getParams(View ll, int db)//visszaadja az n. param�tert
		{
			float[] ret = new float[db];
			for(int i = 0; i < db; i++)
			{
				int elojel_id = getResources().getIdentifier("elojel_" + i, "id", "tami.szakdoga.fuggvenyek");
				float elojel = 1;
				if(elojel_id != 0) //Csak ha van el�jel�nk
					{
					ToggleButton butt = (ToggleButton)ll.findViewById(elojel_id);
					if(butt != null)
						elojel = butt.isChecked() ? 1 : -1;
					}
					
				
				int szam_id = getResources().getIdentifier("param_" + i, "id", "tami.szakdoga.fuggvenyek");
				float param = Float.parseFloat(((EditText)ll.findViewById(szam_id)).getText().toString());
				
				ret[i] = elojel * param;
			}
			return ret;
		}
		
		void SaveSettings()
		{
			try{//Nem �gy szok�s hib�t feldolgozni, de ha m�r k�pes valaki ebben hib�zni az meg�rdemli...
			LinearLayout ll = (LinearLayout) getActivity().findViewById(R.id.settings_layout);
			
			int ind = ((MainActivity)getActivity()).selectedGraph;
			Graph g = ((MainActivity)getActivity()).getGraphs().get(ind);
			
			String type = ((Spinner)ll.findViewById(R.id.tipus)).getSelectedItem().toString();
			
			try{
			g.changePage(Integer.parseInt((((EditText) ll.findViewById(R.id.page_num)).getText().toString())));
			}
			catch(Exception e){}//Ha valahogy siker�lt nem sz�mot megadni...
			
			float[] params = null;
			switch(type)
			{
			case CONST.FV_CONST:
				params = getParams(ll, 1);
				break;
			case CONST.FV_LIN:
				params = getParams(ll, 3);
				break;
			case CONST.FV_NFOK:
				params = getParams(ll, 4);
				break;
			case CONST.FV_GYOK:
				params = getParams(ll, 3);
				break;
			case CONST.FV_ABS:
				params = getParams(ll, 3);
				break ;
			case CONST.FV_EGESZ:
				params = getParams(ll, 3);
				break ;
			case CONST.FV_TORT:
				params = getParams(ll, 3);
				break ;
			case CONST.FV_SIG:
				params = getParams(ll, 3);
				break ;
			}
			
			g.changeType(type, params);
			
			
			//Sz�nbe�ll�t�s
			g.setColor(((Spinner)ll.findViewById(R.id.szin)).getSelectedItemPosition());
			
			((MainActivity)getActivity()).refreshList();//�rtes�ts�k, hogy v�ltozott az �rt�k amit mutatnia kell
			}
			catch(Exception e){Toast.makeText(getActivity(), "Hiba", Toast.LENGTH_LONG).show();}
		}
		
		View LoadGraphPage(Context root, int page)
		{
			GraphView img = new GraphView(root);
			img.setGraphs(((MainActivity) getActivity()).graphs);
			((MainActivity) getActivity()).getGraphData(page-1).LoadTo(img);
			
			graph = img;
			
			return img;
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			Bundle args = getArguments();
			
			spinner_def_select = true;
			
			View rootView = null;
			
			if(args != null)
			{
				try {
					pagenum = args.getInt(CONST.ARG_SECTION_NUMBER);
				} catch(Exception e){}
				
				rootView = LoadPage(inflater, container);
			}
			else
			{
				//Oh-oh
			}
			
			return rootView;
		}
		
		@Override
		public void onPause()
		{
			super.onPause();
			if(pagenum > 2)
				((MainActivity) getActivity()).setGraphData(pagenum -3, new GraphData(graph));
			
			//K�sz�nj�nk el az activityt�l ha mi voltunk a settings page
			if(((MainActivity) getActivity()).settingspage == this)
				((MainActivity) getActivity()).settingspage = null;
		}
	}

}
