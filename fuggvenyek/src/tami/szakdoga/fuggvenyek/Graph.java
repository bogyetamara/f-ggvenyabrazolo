package tami.szakdoga.fuggvenyek;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;


//nem tudom miért nevezem grafikonnak a fv-t, de már ez marad
public class Graph implements Parcelable{
	private String tipus;
	private float[] params;
	private int col = 0;
	
	private int Page = 1;
	
	Graph(String tipus, float[] params)
	{
		this.tipus = tipus;
		this.params = params;
	}
	
	void changeParams(float[] params)
	{
		this.params = params;
	}
	
	float getValueAt(float x)
	{
		switch(tipus)
		{
		case CONST.FV_CONST:
			return(params[0]);
		case CONST.FV_LIN:
			return params[0]* (x + params[1]) + params[2];
		case CONST.FV_NFOK:
			return (float) (params[0]* Math.pow(x + params[1], params[2]) + params[3]);
		case CONST.FV_GYOK:
			return (float) (params[0]* Math.sqrt(x + params[1]) + params[2]);
		case CONST.FV_ABS:
			return (float) (params[0]* Math.abs(x + params[1]) + params[2]);
		case CONST.FV_EGESZ:
			return (float) (params[0]* Math.floor(x + params[1]) + params[2]);
		case CONST.FV_TORT:
			return (float) (params[0]* x + (params[1] - Math.floor(x + params[1])) + params[2]);
		case CONST.FV_SIG:
			return (float) (params[0]* Math.signum(x + params[1]) + params[2]);
		}
		return x*x;
	}
	
	void setColor(int col)
	{
		this.col = col;
	}
	
	int getColor()
	{
		return col;
	}
	
	String getType()
	{
		return tipus;
	}
	
	void changeType(String type, float[] params)
	{
		this.tipus = type;
		this.params = params.clone();
	}
	
	int getPage()
	{
		return Page;
	}
	
	float[] getParams()
	{
		return params.clone();
	}
	
	void setParam(int i, float val)
	{
		if(params.length > i)
			params[i] = val;
	}
	
	void changePage(int newpage)
	{
		Page = newpage;
	}
	
	@Override
	public String toString()
	{
		String text = "";
		switch(tipus)
		{
		case CONST.FV_CONST:
			text += params[0];
			break;
		case CONST.FV_LIN:
			text += params[0] + "(x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " ) " + (params[2] >= 0 ? (" + " + params[2]) : (" - " + -params[2]));
			break;
		case CONST.FV_NFOK:
			text += params[0] + "(x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " ) ^ " + params[2] + (params[3] >= 0 ? (" + " + params[3]) : (" - " + -params[3]));
			break;
		case CONST.FV_GYOK:
			text += params[0] + "√(x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " )" + (params[2] >= 0 ? (" + " + params[2]) : (" - " + -params[2]));
			break;
		case CONST.FV_ABS:
			text += params[0] + "|x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " |" + (params[2] >= 0 ? (" + " + params[2]) : (" - " + -params[2]));
			break;
		case CONST.FV_TORT:
			text += params[0] + "{x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " }" + (params[2] >= 0 ? (" + " + params[2]) : (" - " + -params[2]));
			break;
		case CONST.FV_EGESZ:
			text += params[0] + "[x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " ]" + (params[2] >= 0 ? (" + " + params[2]) : (" - " + -params[2]));
			break;
		case CONST.FV_SIG:
			text += params[0] + "sign(x" + (params[1] >= 0 ? (" + " + params[1]) : (" - " + -params[1])) + " )" + (params[2] >= 0 ? (" + " + params[2]) : (" - " + -params[2]));
			break;
		}
		return text;
	}
	
	Graph Clone()
	{
		Graph ret = new Graph(tipus, params.clone());
		ret.Page = Page;
		ret.col = col;
		return ret;
	}
	
	//Parcelable - tömörít menthető alakba
	
	 protected Graph(Parcel in) {
		 tipus = in.readString();
	     col = in.readInt();
	     Page = in.readInt();
	     in.readFloatArray(params);
    }

	 @Override
	 public int describeContents() {
		 return 0;
	 }

	    @Override
	    public void writeToParcel(Parcel dest, int flags) {
	        dest.writeString(tipus);
	        dest.writeInt(col);
	        dest.writeInt(Page);
	        dest.writeFloatArray(params);
	    }

	    public static final Parcelable.Creator<Graph> CREATOR = new Parcelable.Creator<Graph>() {
	        @Override
	        public Graph createFromParcel(Parcel in) {
	            return new Graph(in);
	        }

	        @Override
	        public Graph[] newArray(int size) {
	            return new Graph[size];
	        }
	    };
}
