package tami.szakdoga.fuggvenyek;

import android.graphics.Color;

public class CONST {
	public static final String ARG_SECTION_NUMBER = "section_number";
	public static final String ARG_FV_NEV = "fuggv_nev";
	public static final String ARG_FV_PARAM = "fuggv_param";
	public static final String FV_CONST = "Konstans";
	public static final String FV_LIN = "Line�ris";
	public static final String FV_NFOK = "n. fok�";
	public static final String FV_GYOK = "Gy�k";
	public static final String FV_ABS = "Abszol�t�rt�k";
	public static final String FV_EGESZ = "Eg�szr�sz";
	public static final String FV_TORT = "T�rtr�sz";
	public static final String FV_SIG = "Signum";
	public static final int COLORS[] = {Color.WHITE, Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.GRAY};
}
