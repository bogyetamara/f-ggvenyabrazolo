package tami.szakdoga.fuggvenyek;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PointF;
import android.graphics.Bitmap.Config;

@SuppressLint("ClickableViewAccessibility")
public class GraphView extends ImageView implements OnTouchListener{
	
	private ArrayList<Graph> grafs = new ArrayList<Graph>();
	private int page = 1;
	
	Bitmap bm;
	
	public GraphView(Context context) {
		super(context);
		setOnTouchListener(this);
		paint = new Paint();
		paint.setTextAlign(Align.CENTER);
		// TODO Auto-generated constructor stub
	}

	public GraphView(Context context, AttributeSet set) {
		super(context, set);
		setOnTouchListener(this);
		paint = new Paint();
		// TODO Auto-generated constructor stub
	}
	
	Canvas canv;
	Paint paint;
	
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;
	PointF start = new PointF();
	PointF mid = new PointF();
	
	PointF kzp_old = new PointF();
	PointF kzp = new PointF();
	float oldDist;
	
	float zoom_old;
	float zoom = 1;
	
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);
	}
	
	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {		
		switch (event.getAction() & MotionEvent.ACTION_MASK)
		{
		case MotionEvent.ACTION_DOWN:
			kzp_old = new PointF(kzp.x, kzp.y);
			start.set(event.getX(), event.getY());
			mode = DRAG;
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			mode = NONE;
			break;
		
		case MotionEvent.ACTION_POINTER_DOWN:
			oldDist = spacing(event);
			zoom_old = zoom;
			if (oldDist > 10f) {
			midPoint(mid, event);
			mode = ZOOM;
			}
			break;
		
		case MotionEvent.ACTION_MOVE:
			if (mode == DRAG)
			{
				kzp = new PointF(kzp_old.x + (event.getX()-start.x)/getWidth()/zoom, kzp_old.y + (event.getY()-start.y)/getWidth()/zoom);
			}
			else if (mode == ZOOM)
			{
				float newDist = spacing(event);
				if (newDist > 10f)
				{
					float scale = newDist / oldDist;
					zoom = zoom_old * scale;
				}
				PointF mid_new = new PointF();
				midPoint(mid_new, event);
				
				kzp.x = (mid_new.x/getWidth()-0.5f)/zoom - getXFromPixel(mid.x, zoom_old, kzp_old); 
				kzp.y = (mid_new.y-getHeight()/2)/getWidth()/zoom + getYFromPixel(mid.y, zoom_old, kzp_old);;
			}
			break;
		}
		invalidate();
		return true;
	}

	
	public void setGraphs(ArrayList<Graph> fvek)
	{
		grafs = fvek;
	}
	
	public void setPage(int page)
	{
		this.page = page;  
	}
	
	@Override
	protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
		super.onSizeChanged(xNew, yNew, xOld, yOld);
	     
	    bm = Bitmap.createBitmap(xNew, yNew, Config.ARGB_8888);
	    
	    canv = new Canvas();
		canv.setBitmap(bm);
		canv.drawColor(Color.BLACK);
	
		this.setImageBitmap(bm);
	}
	
	
	//Ezek arra, hogy lehessen rendesen oda-vissza v�ltani koordin�t�kat
	float getXPixel(float x, float zoom, PointF kzp)
	{
		return ((kzp.x + x) * zoom + 0.5f)*getWidth() ;
	}
	float getXPixel(float x){return getXPixel(x, zoom, kzp);}
	
	float getYPixel(float y, float zoom, PointF kzp)
	{
		return (kzp.y - y) * zoom*getWidth() + getHeight()/2;
	}
	float getYPixel(float y){return getYPixel(y, zoom, kzp);}
	
	float getXFromPixel(float x, float zoom, PointF kzp)
	{
		return (x/getWidth()-0.5f)/zoom - kzp.x;
	}
	float getXFromPixel(float x){return getXFromPixel(x, zoom, kzp);}
	
	float getYFromPixel(float y, float zoom, PointF kzp)
	{
		return -((y-getHeight()/2)/getWidth()/zoom - kzp.y);
	}
	float getYFromPixel(float y){return getYFromPixel(y, zoom, kzp);}
	
	///Kerek�t az n. jegyig (0.10000001 -> -1)
	static float kerekit(float x, int n)
	{
		final double nagysagrend = Math.pow(10, -n);
		final double ert = Math.round(x * nagysagrend);
		return (float) (ert / nagysagrend);
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		//super.onDraw(canvas);
		
		canvas.drawColor(Color.BLACK);
	    paint.setStyle(Paint.Style.FILL);
		
	    paint.setColor(Color.WHITE);
		
	    ///tengelyek
	    float x_0 = getXPixel(0);
	    float y_0 = getYPixel(0);
	    canvas.drawLine(x_0, 0, x_0, getHeight(), paint);
	    canvas.drawLine(0, y_0, getWidth(), y_0, paint);
	    
	    //tengelyfelirat
	    int n = (int) Math.floor(Math.log10(1/zoom));
	    
	    float z = (float) Math.pow(10,n);
	    if(1/zoom / z < 7)
	    	if(1/zoom / z < 2)
	    		z /= 5;
	    	else if (1/zoom / z < 2.5f)
	    		z /= 4;
	    	else
	    		z /= 2;
	    
	    int kezd_x = (int) Math.floor((-kzp.x - 1/zoom/2) / z);
	    int kezd_y = (int) Math.floor((kzp.y - 1/zoom/2* getHeight() / getWidth()) / z)-2;
	    //x tengely
	    for(float i = 0; i <= 1/ zoom / z +1; i++)
	    {
	    	float poz = (float) ((kezd_x+i) * z);
	    	
	    	float y_pos = (kzp.y < 1/zoom/2*getHeight() / getWidth()) ? y_0+10 : getHeight()-10;
	    	if(kzp.y < -1/zoom/2*getHeight() / getWidth()) y_pos = 10;
	    	
	    	canvas.drawText("" + kerekit(poz, n-1), getXPixel((float)poz), y_pos, paint);	
	    }
	    //y tengely
	    for(float i = 0; i <= 1/ zoom / z * getHeight() / getWidth() +5; i++)
	    {
	    	float poz = (float) ((kezd_y+i) * z);
	    	float x_pos = (kzp.x < 1/zoom/2) ? x_0+10 : getWidth()-50;
	    	if(kzp.x < -1/zoom/2) x_pos = 10;
	    	canvas.drawText("" + kerekit(poz, n-1), x_pos, getYPixel((float)poz), paint);
	    }
	    
	    
	    for(int i = 0; i < grafs.size(); i++)
	    {
	    	//Csak akkor kell ez a fv ha ezen az oldalon van
	    	if(grafs.get(i).getPage() == page)
	    	{
				paint.setColor(CONST.COLORS[grafs.get(i).getColor()]);
				for(float x = 0; x <= getWidth(); x += 1)
				{
					try{//<- �tmeneti megold�s - gy�k�k probl�m�sak - nincs �rtelmezve negat�vakon
						
					Graph g = grafs.get(i); 
					float x1 = (x/getWidth()-0.5f)/zoom - kzp.x, x2 = ((x+1)/getWidth()-0.5f)/zoom - kzp.x;
					float y1 = (g.getValueAt(x1) - kzp.y)*zoom, y2 = (g.getValueAt(x2)-kzp.y)*zoom;
					
					//figyelj�nk a szakad� f�ggv�nyekre, azokn�l nincsenek itt vonalak - ha nagyobbat ugrik a fv mint param[0]/2 akkor ott szakad�s lesz
					if((g.getType().equals(CONST.FV_EGESZ) || g.getType().equals(CONST.FV_TORT) || g.getType().equals(CONST.FV_SIG)) && Math.abs(g.getValueAt(x2) - g.getValueAt(x1)) > Math.abs(g.getParams()[0]) / 2 && g.getParams()[0] != 0)
						continue;
					
					canvas.drawLine(x, getHeight()/2-y1*getWidth(), x + 1, getHeight()/2 - y2*getWidth(), paint);
					}
					catch(Exception e){}
				}
	    	}
	    }
	}
	
	//Egy oszt�ly ami k�pes elt�rolni a GraphView minden extra inform�ci�j�t , illetve azt kiolvasni / bet�lteni mik�zben Parcelable
	public static class GraphData implements Parcelable {
		private int page = 1;
		float kzp_x = 0;
		float kzp_y = 0;
		float zoom = 1;
		
		GraphData(GraphView g)
		{
			page = g.page;
			kzp_x = g.kzp.x;
			kzp_y = g.kzp.y;
			zoom = g.zoom;
		}
		
		void LoadTo(GraphView g)
		{
			g.page = page;
			g.kzp = new PointF(kzp_x, kzp_y);
			g.zoom = zoom;
			g.invalidate();
		}
		
		protected GraphData(Parcel in) {
			page = in.readInt();
			kzp_x = in.readFloat();
			kzp_y = in.readFloat();
			zoom = in.readFloat();
		}

	    public GraphData(int page) {
	    	this.page = page;
			// TODO Auto-generated constructor stub
		}

		@Override
		public int describeContents() {
	    	return 0;
		}
	
	    @Override
	    public void writeToParcel(Parcel dest, int flags) {
	    	dest.writeInt(page);
	    	dest.writeFloat(kzp_x);
	    	dest.writeFloat(kzp_y);
	    	dest.writeFloat(zoom);
	    }
	
	    public static final Parcelable.Creator<GraphData> CREATOR = new Parcelable.Creator<GraphData>() {
			@Override
			public GraphData createFromParcel(Parcel in) {
				return new GraphData(in);
			}
	
			@Override
			public GraphData[] newArray(int size) {
				return new GraphData[size];
			}
		};
	}
	
}
